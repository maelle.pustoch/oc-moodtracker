-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:8889
-- Généré le :  mer. 27 nov. 2019 à 14:27
-- Version du serveur :  5.7.25
-- Version de PHP :  7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de données :  `moodtracker`
--

-- --------------------------------------------------------

--
-- Structure de la table `mood`
--

CREATE TABLE `mood` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `mood_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `mood` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `mood`
--

INSERT INTO `mood` (`id`, `user_id`, `mood_date`, `mood`) VALUES
(1, 109, '2019-11-27 13:23:41', 2),
(2, 109, '2019-11-27 13:23:41', 3),
(3, 109, '2019-11-27 13:23:42', 4),
(4, 109, '2019-11-27 13:23:42', 5),
(5, 109, '2019-11-27 13:23:43', 1),
(6, 109, '2019-11-27 13:23:43', 1),
(7, 109, '2019-11-27 13:23:44', 2),
(8, 109, '2019-11-27 13:23:45', 3),
(9, 109, '2019-11-27 13:23:45', 4);

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `passwordconf` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`user_id`, `name`, `lastname`, `email`, `username`, `password`, `passwordconf`) VALUES
(109, 'maelle', 'Pustoc\'h', 'maelle@gmail.com', 'maelle', 'kilini', 'kilini'),
(173, 'Raphael', 'fafafa', 'fafa@yopmailo.com', 'fafa', 'fafa', 'fafa'),
(174, 'esmée', 'toutou', 'esmee@lycos.com', 'toutou', 'toutou', 'toutou');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `mood`
--
ALTER TABLE `mood`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id_2` (`user_id`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`),
  ADD KEY `user_id` (`user_id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `mood`
--
ALTER TABLE `mood`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=176;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `mood`
--
ALTER TABLE `mood`
  ADD CONSTRAINT `mood_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`);

--
-- Contraintes pour la table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `mood` (`user_id`);

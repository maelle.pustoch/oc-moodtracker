<?php
namespace App\Models;

class User extends Databasemanager
{
	public $db;
	public $user_id;
	public $name;
	public $lastname;
	public $email;
	public $username;
	public $password;

	public function userExists()
	{
	  $db = $this->dbConnect();	
	  $rq = $db->prepare('SELECT * FROM user WHERE username = ? OR email = ?');
	 	$rq->execute(array($this->username, $this->email));
	 	$userCheck = $rq->fetchAll();
	 	$userChecked = count($userCheck);
	 	
	 	return $userChecked;
	}


	public function getUser()
	{
		$db = $this->dbConnect();
		$rq = $db->prepare('SELECT * FROM user WHERE username = ? AND password = ?');
		$rq->execute(array($this->username, $this->password));
		$getUser = $rq->fetchAll();
		return $getUser;
	}

	public function getUserDetails()
	{
		$db = $this->dbConnect();
		$rq = $db->prepare('SELECT * FROM user WHERE user_id = ?');
		// var_dump((array($this->username, $this->password)));
		$rq->execute(array($this->user_id));
		$getUserDetails = $rq->fetchAll();
		return $getUserDetails;
	}

	public function userUpdate() 
	{
		$db = $this->dbConnect();		
		$rq = $db->prepare('UPDATE user SET name = ?, lastname = ?, email = ?, username = ?, password = ? WHERE user_id = ?' );
		$updatedUser = $rq->execute(array($this->name, $this->lastname, $this->email, $this->username, $this->password, $this->user_id));
		return $updatedUser;

	}

	public function signup()
	{
    $db = $this->dbConnect();		
		$rq = $db->prepare('INSERT INTO user (name, lastname, email, username, password) VALUES(?, ?, ?, ?, ?)');
	$signup = $rq->execute(array($this->name, $this->lastname, $this->email, $this->username, $this->password));

		if ($signup) { 
      	return $db->lastInsertId();
		} else {
			return false;
		}
	}
}
<?php
namespace App\Models;


class Mood extends Databasemanager
{
	public $db;
	public $user_id;
	public $mood_date;
	public $mood;


	public function setMood()
	{
    $db = $this->dbConnect();		
		$rq = $db->prepare('INSERT INTO mood (mood, user_id) VALUES (?, ?)');
		$moodSelect = $rq->execute(array($this->mood, $this->user_id));
		return $moodSelect;
	}
	

	public function getMood()
	{
		$db = $this->dbConnect();	
		$rq = $db->prepare('SELECT user_id, mood_date, mood FROM mood ORDER BY mood_date DESC');
		$rq->execute();
		return $rq->fetchAll();
	}


	public function getStats()
	{
		$db = $this->dbConnect();	
		$rq = $db->prepare('SELECT * FROM mood WHERE user_id = ?');
		$moodChart = $rq->execute(array($this->user_id));

		return $rq->fetchAll();		
	}

	public function getTotalStats()
	{
		$db = $this->dbConnect();	
		$rq = $db->prepare('SELECT mood, COUNT(mood) AS Total FROM mood WHERE user_id = ? GROUP BY mood ');
		$moodSum = $rq->execute(array($this->user_id));
		$moodTotalSum = $rq->fetchAll(\PDO::FETCH_ASSOC);
		return $moodTotalSum;		
	}



	public function getSumStats ($start, $end) 
	{
		$db = $this->dbConnect();

		$mysqlstart = date("Y-m-d H:m:s", $start);
		$mysqlend = date("Y-m-d H:m:s", $end);

		$rq = $db->prepare('SELECT mood_date, mood FROM mood WHERE user_id = ? AND mood_date >= ? AND mood_date <= ? ');
		$moodSum = $rq->execute(array($this->user_id, $mysqlstart, $mysqlend));
		$moodTotalSum = $rq->fetchAll(\PDO::FETCH_ASSOC);

		
		return $moodTotalSum;
	}


	public function getAvgStats()
	{
		$db = $this->dbConnect();	
		$rq = $db->prepare('SELECT user_id, AVG(mood) FROM mood  WHERE user_id = ? ');
		$moodChart = $rq->execute(array($this->user_id));
		$moodAverage = $rq->fetchAll();
		return $moodAverage ;
		
	}


	public function getMonth()
	{
		$db = $this->dbConnect();	
		$rq = $db->prepare('SELECT MONTH(mood_date) FROM `mood` WHERE user_id = ? ORDER BY mood_date DESC ');
		$moodMonth = $rq->execute(array($this->user_id));
		$monthAverage = $rq->fetch();
		return $monthAverage ;
	}


	public function getStacks()
	{
	$db = $this->dbConnect();	
	$rq = $db->prepare('SELECT
	    MONTH(mood_date) AS mood_month,	    
	    (SELECT
	     	COUNT(mood) FROM mood
	     WHERE user_id = :userid
	     	and month(mood_date) = mood_month
	     	and mood=1) as depressed,
	    (SELECT
	     	COUNT(mood) FROM mood
	     WHERE user_id = :userid
	     	and month(mood_date) = mood_month
	     	and mood=2) as sad,
	    (SELECT
	     	COUNT(mood) FROM mood
	     WHERE user_id = :userid
	     	and month(mood_date) = mood_month
	     	and mood=3) as okay,
	    (SELECT
	     	COUNT(mood) FROM mood
	     WHERE user_id = :userid
	     	and month(mood_date) = mood_month
	     	and mood=4) as happy,
	     (SELECT
	     	COUNT(mood) FROM mood
	     WHERE user_id = :userid
	     	and month(mood_date) = mood_month
	     	and mood=5) as joyful
	        
	FROM mood
	WHERE user_id = :userid
	GROUP BY mood_month');
		$user = $rq->execute(array(":userid" => $this->user_id));
		$getStacks = $rq->fetchAll(\PDO::FETCH_ASSOC);
			
	return $getStacks ;

	
	}
}




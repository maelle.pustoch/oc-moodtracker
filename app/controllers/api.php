<?php 

require_once('../app/models/Mood.php');

class API extends Controller

{

	public function getStats($name = '')
	{
		error_reporting(E_ALL & ~E_NOTICE);

		$statsMood = new App\Models\Mood();
		$start = $_GET['start'];
		$end = $_GET['end'];
		if($start === NULL) {
			$start = time()-7*24*60*60;
		}

		if($end === NULL) {
			$end = time();
		} 

		$statsMood->user_id = $_SESSION['user_id'];
		$raw = $statsMood->getSumStats($start, $end);
		$tab = ['labels' => [], 'data' => []];
	
		foreach ($raw as $value) {
			array_push($tab['labels'], $value['mood_date']);
			array_push($tab['data'], (int) $value['mood']);
		}
		header('Content-Type: application/json');
		echo json_encode($tab);
	}


	public function getAverageStats($name = '') 
	{
		$statsMood = new App\Models\Mood();

		$statsMood->user_id = $_SESSION['user_id'];
		header('Content-Type: application/json');
		$raw = $statsMood->getTotalStats();

		$tab = ['label' => [], 'data' => []];

		foreach ($raw as $value) {
			array_push($tab['label'], (int)$value['mood']);
			array_push($tab['data'], $value['Total']);	
		}  
		echo json_encode($tab);
	}


	public function getAverageMonthStats($name = '') 
	{
		$statsMood = new App\Models\Mood();
		$statsMood->user_id = $_SESSION['user_id'];
		header('Content-Type: application/json');
		
		$raw = $statsMood->getStacks();
		$tab = ['label' => [], 'data' => []];
		
		foreach ($raw as $month) {
			array_push($tab['label'], (int) $month['mood_month']);
			foreach ($month as $mood_name => $total_for_month) {

				// We don't want the column 'mood_month'
				if($mood_name !== 'mood_month') {
					if(!isset($tab['data'][$mood_name])) {
						$tab['data'][$mood_name] = [];
					}
					
					array_push($tab['data'][$mood_name], $total_for_month);
				}
			}
		}

		echo json_encode($tab);
	}

}






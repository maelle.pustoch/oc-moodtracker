<?php 

ob_start();

require_once('../app/models/User.php');
require_once('../app/models/Mood.php');
require_once('../app/core/Mail.php');
require_once('../config.php');


class User extends Controller 
{
	
	public function usersignup() 
	{
		$this->view('user/signup', []); 
	}


	public function userchecked() 
	{
		$this->view('user/userchecked', []); 
	}


	public function usersignin() 
	{
		$this->view('user/signin', []); 

	}

	public function useractivity() 
	{
		$this->view('user/activity', []); 

	}

	public function nothingthere() 
	{
		$this->view('user/nothingthere', []); 
	}

	public function settings() 
	{

		try {
				$user = new App\Models\User();

				$user->user_id = $_SESSION['user_id'];
				$userDetails = $user->getUserDetails();

				$name = $userDetails[0]["name"];
				$lastname = $userDetails[0]["lastname"];
				$email = $userDetails[0]["email"];
				$username = $userDetails[0]["username"];
				$password = $userDetails[0]["password"];

		 		if(isset($_POST['Save'])) {
					$user->name = $_POST["name"];
					$user->lastname = $_POST["lastname"];
					$user->email = $_POST["email"];
					$user->username = $_POST["username"];
					if(!empty($_POST['password'])) {
						$user->password = sha1($_POST["password"]);
					} else {
						$user->password = $password;
						

					}
					
					$user->user_id = $_SESSION['user_id'];
					$updateUser= $user->userUpdate();
					$mail = new Mail();
					$mail->send($user->email, 'Change of Account Details', 'Your account details have been saved');
					$_SESSION['mailSettings'] = "Changes well registered. We sent you a confirmation mail !";

				$name = $user->name;
				$lastname = $user->lastname;
				$email = $user->email;
				$username = $user->username;
				$password = $user->password;
				$_SESSION['username'] = $user->username;

				}

				$this->view('user/settings', [
					'name' => $name,
					'lastname'=> $lastname,
					'email' => $email,
					'username' => $username,
					'password' => $password,

				]);
			
			} 
			catch (Exception $e) {

				$_SESSION['mailNotSent'] = "Confirmation email cannot be sent. Have you checked your email adress ?";

			}

		}
	
	private function eqMonth($monthNumber)
	{
		if(isset($_SESSION['user_id'])) {
			if(!empty($monthNumber)) {
				$monthEq = [
					1=>'January',
					2=>'February',
					3=>'March',
					4=>'April',
					5=>'May',
					6=>'June',
					7=>'July',
					8=>'August',
					9=>'September',
					10=>'October',
					11=>'November',
					12=>'December'
				];
				return $monthEq[$monthNumber];
			} 
		}
	}
	

	private function eqAvg($moodNumber)
	{
		if(isset($_SESSION['user_id'])) {
			if(!empty($moodNumber)) {
					$moodEq = [
						1=>'Depressed',
						2=>'Sad',
						3=>'Okay',
						4=>'Happy',
						5=>'Joyful',
					];
				return $moodEq[$moodNumber];
			} 
		}
	}
	

	public function account() 
	{
		$newMood = new App\Models\Mood();
		$user = new App\Models\User();

		if(isset($_SESSION['user_id'])) {
			$newMood->user_id = $_SESSION['user_id'];
			$average = $newMood->getAvgStats();
			$moodAvg = round($average[0][1]);
			$currentMood = $this->eqAvg($moodAvg);
			
			$monthTest = $newMood->getStats(); 
			// $currentMonth = $monthTest[0][2];
			$month = $newMood->getMonth();
			$monthAvg = $month[0];
			$currentMonth= $this->eqMonth($monthAvg);

			$this->view('user/account', [
				'average' => $currentMood,
				'month' => $currentMonth
			]);
		} 
	}


	public function signin() 
	{   
		$registeringUser = new App\Models\User();

		if(isset($_POST['sign-in']) && !empty($_POST['username']) && !empty($_POST['password'])) {
			$registeringUser->username = $_POST['username'];
			$registeringUser->password = sha1($_POST['password']);
			$user = $registeringUser->getUser();
			$registeringUser->user_id = $user[0]['user_id'];
			
				if(count($user) > 0) {	
					$_SESSION['userlog'] = 1;
					$_SESSION['username'] = $registeringUser->username;
					$_SESSION['user_id'] = $registeringUser->user_id;
					header('Location: /user/account');
					exit;
				
				} else if(count($user) <= 0) {
				$_SESSION['userexists'] = "Wrong password or username. Do you have an account ? <a href='/user/usersignup'class='alert-link'>Sign up</a>";
				header('Location: /user/usersignin');
				exit;
			}
		}
	}

	public function signup()
	{  
		try {
			$sessData = !empty($_SESSION['userlog']) ? $_SESSION['userlog'] : 'Vous êtes déja connecté';
			$newUser = new App\Models\User();

		  if(!empty($_POST['firstname']) && !empty($_POST['lastname']) && !empty($_POST['email']) && !empty($_POST['username']) && !empty($_POST['password']) && !empty($_POST['passwordconfirm'])) {
			   	if($_POST['password'] !== $_POST['passwordconfirm']) { 
			   			$_SESSION['errorpasswordmatch'] = "Password must match !";
				header('Location: /user/usersignup');
				exit;
			   		}

		   		elseif(isset($_POST['sign-up'])) {	
			   		$newUser->name = $_POST['firstname'];
						$newUser->lastname = $_POST['lastname'];
				  	$newUser->email = $_POST['email'];
				  	$newUser->username = $_POST['username'];
				  	$newUser->password = sha1($_POST['password']);
				  	$user = $newUser->getUser();



			  	if($newUser->userExists() > 0 )  {
		  			$_SESSION['userlog'] = 0;
		  			$_SESSION['useralreadyexists'] = "User already exists ! Connect to your account";
		  			header('Location: /user/usersignup');
		  			exit;
						
					} else {
						$signup = $newUser->signup();
	  				$_SESSION['userlog'] = 1;
						$_SESSION['user_id'] = $signup; 
						$_SESSION['username'] = $newUser->username;
						$mail = new Mail();
						$mail->send($newUser->email, 'Welcome on emotions', 'Welcome on emotions, Your registration have been well received');
		   			header('Location: /user/userchecked');
		   			exit;
	   			}
			  }
			}	
	  } catch (Exception $e) {
	  	$_SESSION['WelcomeMailNotSent'] = "Confirmation email cannot be sent. Have you checked your email adress ? ";
	  }  		
	}

	public function setMood()
	{
		$newMood = new App\Models\Mood();
		$user = new App\Models\User();

		if(isset($_SESSION['user_id'])) {
			$newMood->mood = $_GET['mood'];
			$newMood->user_id = $_SESSION['user_id'];
			// $getMood = $newMood->getMood();
			// $newMood->user_id = $_SESSION['user_id'];
			// $newMood->mood = $_GET['mood'];
			$setMood = $newMood->setMood();

			if ($setMood === true) {
				$_SESSION['moodValidate'] =  "Well done !" .'<br>'. '<a class="btn btn-light border-0 mt-4 px-4" href= "/user/account" >Dashboard</a>';
				header('Location: /');
				exit;

			}	else {
				$_SESSION['moodError'] = "Connect to your account to register your mood !" .'<br>'. '<a class="btn btn-outline-light border-0 mt-4 px-4" href= "/user/usersignin">Sign in</a>';
				header('Location: /');
				exit;
			}
		}	else {
			$_SESSION['moodError'] = "Connect to your account to register your mood !" .'<br>'. '<a class="btn btn-light border-0 mt-4 px-4" href= "/user/usersignin">Sign in</a>';
				header('Location: /');
				exit;
		}
	}



	public function logOut() 
	{
  	if(isset($_SESSION['user_id'])) {   	
  		session_destroy();
  	  header('Location: /');
  	} 
	}





}





 

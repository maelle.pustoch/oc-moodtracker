<?php 

class Home extends Controller 
// extends -> Home herite des methodes de Controller
{
	public function index ($name = '') 
	{
		$user = $this->model('User'); // j'ai accés à ma fonction model dans laquel je lui passe le parametre 'User'
		$user->name = $name; // ?
		$this->view('home/homepage', ['name' => $user->name]); // retourne la fonction view() qui va prendre une $view et une $data sous forme de tableau
	}
}
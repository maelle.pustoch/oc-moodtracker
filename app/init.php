<?php

error_reporting(E_ALL);
require_once('../config.php');
require_once('../database.php');
require_once ('core/App.php');  // verifie que le fichier App soit inclu
require_once ('core/Controller.php'); // verifie que le fichier Controller soit inclu

// require_once vérifie si le fichier a déjà été inclus, et si c'est le cas, ne l'inclut pas une deuxième fois.


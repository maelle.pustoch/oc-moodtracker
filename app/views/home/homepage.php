<?php
include('../app/views/include/layout_header.php');
include('../app/views/include/menuTopBar.php');

error_reporting(E_ALL);?>

<body class="text-center img-bg" >
	
	<section>
   <div class="cover-container mt-5 d-flex h-100 p-5 mx-auto flex-column rounded"  >
  	<main role="main" class="inner cover text-white">
	    <h1 class="cover-heading mt-5"><?php if(isset($_SESSION['username'])) { echo 'Hello ' .$_SESSION['username']. ''; } else { echo 'Hello'; } ?></h1>
	    <h3 class="text-white mt-4 mb-4">How do you feel Today ?</h3>
	    
	    <div class = "row boutons mt-lg-5 my-5">
				<div class =  "col-xs-8 col-sm col-md-12 col-lg-12 col-xl-12 u-pointer button-section  text-center justify-content-center hello-section text-white">
					<a type="button btn button" class="mood btn btn-secondary border-0 mt-2"style = "background: #213E77;" href = "/user/setMood?mood=1" value = 1>
						<img class = "pt-3 text-white" src="/img/depressed.svg" alt="depressed" value = 2>
					</a>
					<a type="btn button" class="mood btn btn-secondar border-0 mt-2" style = "background: #45BDC5;" href = "/user/setMood?mood=2" value = 2>
						<img class = "pt-3 text-white" src="/img/sad.svg" alt="sad" >
					</a>
					<a type="btn button" class="mood btn btn-secondary border-0 mt-2" style = "background: #8CC545;" href = "/user/setMood?mood=3" value = 3>
						<img class = "pt-3 text-white" src="/img/okay.svg" alt="okay">
					</a>
					<a type="btn button" class="mood btn btn-secondary border-0  mt-2" style = "background: #FFCD1B;" href = "/user/setMood?mood=4" value = 4>
						<img class = "pt-3 text-white" src="/img/happy.svg" alt="happy" >
					</a>
					<a type="btn button" class="mood btn btn-secondary border-0  mt-2" style = "background:#CC0995;" href = "/user/setMood?mood=5" value = 5>
						<img class = "pt-3 text-white" src="/img/joyful.svg" alt="joyful" >
					</a>
				</div>
			</div>
	  </main>
  		<p class = "notif text-white">
		  	<?php if(isset($_SESSION['moodValidate'])) { echo $_SESSION['moodValidate']; 
		  		unset($_SESSION['moodValidate']);} 
		  	  if (isset($_SESSION['moodError'])) { echo $_SESSION['moodError'];
		  		unset($_SESSION['moodError']); }
		  	?>
	  	</p> 
	 </div> 
	</section>
</body>








<?php include('../app/views/include/layout_footer.php'); ?>


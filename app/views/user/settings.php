<?php include('../app/views/include/layout_header.php'); ?>
<?php include('../app/views/include/topnav.php'); ?>

<nav>
	<div class = "float-left col-1 d-none d-sm-none d-md-block">
	<?php include('../app/views/include/sidenav.php'); ?>
	</div>
</nav>

<header>
	<div class = "container-fluid">
		<div class = "row">
			<?php if(isset($_SESSION['mailSettings'])) {?>
			<div class = "col-12 mt-2 d-flex justify-content-end">
				<div class=" w-25 alert alert-success" role="alert">
				  <?php echo $_SESSION['mailSettings']; unset($_SESSION['mailSettings']); ?>
				</div>
			</div>
		<?php } ?>

		<?php if(isset($_SESSION['mailNotSent'])) {?>
			<div class = "col-12 mt-2 d-flex justify-content-end">
				<div class=" w-25 alert alert-danger" role="alert">
				  <?php echo $_SESSION['mailNotSent']; unset($_SESSION['mailNotSent']); ?>
				</div>
			</div>
		<?php } ?>
		</div>
	</div>
</header>

<section>
<div class = "container-fluid mt-3">
	<div class = "row">
		<div class = "col-lg-5 col-md-8 col-xl-5 col-sm-10">
			<h3> Settings</h3>
		
			<hr>	 	
	    <div class = "row mt-5">
  			<div class = "col-12">
	    		<h4>Personnal details </h4>
			    	<form action="/user/settings" method="POST">
						<div class="form-group">
							<label for="lastname" class = "mt-2"></label>
							<input type="text" class="form-control" aria-label="Sizing example input" id = "FamilyNameInput" aria-describedby="inputGroup-sizing-default" value="<?php echo $data['lastname']; ?>" name = "lastname">
							<label for="name" class = "mt-2"></label>
							<input type="text" class="form-control" aria-label="Sizing example input" id = "NameInput" aria-describedby="inputGroup-sizing-default" value="<?php echo $data['name']; ?>" name = "name">									
						    <label for="email" class = "mt-2"></label>
						    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="<?php echo $data['email']; ?>" name = "email">
						    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>

						  <div class="form-group">
						    <label for="exampleInputUsername"></label>
						    <input type="username" class="form-control" id="username" value="<?php echo $data['username']; ?>" name = "username">
						    <label for="exampleInputPassword1"class = "mt-2"></label>
						    <input type="password" class="form-control" id="password" name = "password">
<!-- 						    <label for="exampleInputPassword1"class = "mt-2"></label>
						    <input type="password" class="form-control" id="PasswordConf" value="<?php echo $data['passwordconf']; ?>" name = "passwordconf"> -->
						  </div>						  
						</div>
						<button class="btn btn-info mt-5 px-4" type="submit" id = "Save" name = "Save">Save</button>
					</form>
				</div>
			</div>
  		</div>
		</div>
	</div>
</div>
<section>










<?php include('../app/views/include/layout_footer.php'); ?>
  <?php include('../app/views/include/layout_header.php'); ?>
  <?php include('../app/views/include/menuTopBar.php'); ?>

<div class = "container-fluid">
	<section class="jumbotron text-center">
    <div class="container">
    	<header>
    		<h1 class="text-white">emotions</h1>
    	</header>
      
      <p class="lead text-muted text-white"> Monitor your mood and follow your progresses.</p>
      <p>
      </p>
    </div>
  </section>

	<section>
		<div class = "container mt-5 we-do" id = "activity">			
			<div class = "row text-center justify-content-center">
				<div class = "col-xs-10 col-xl-8 text-justify">
					<h2 class = "text-center text-secondary">What we do</h2>
					<br>
					<p class = "text-center">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
					<br>
					<br>
					Lorem ipsum dolor sit amet, consectetur adipiscing elit</p><br>
				</div>
			</div>
			<hr>
			<div class="row featurette mt-5">
	      <div class="col-md-7 mt-5">
	        <h3 class="featurette-heading"> Pick a Mood. <!-- <span class="text-muted">It’ll blow your mind.</span> --></h3>
	        <p>Feeling a bit low or amazingly happy ? Pick an emotion which captures your mood at any point throughout the day. We believe in that a visual can have much more impact than a thousand words. Data collected will help you to get an in-depth understanding of your habits. It as simple as it looks so just press a mood and we'll do the rest ! </p>
	      </div>
	      <div class="col-md-5">
	        <div class="img-responsive rounded featurette-image" style = "background-image: url('/img/mood.jpg');"></div>
	      </div> <!-- /img/mood.jpg -->
		  </div>

		  <hr>

			<div class="row featurette">
		    <div class="col-md-7 order-md-2 mt-5">
		      <h3 class="featurette-heading ">Personal Dashboard. <!-- span class="text-muted">Monitor your progresses.</span> --></h3>
		      <p> You just registered your first mood ? Brilliant ! Now, log onto your dashboard to track your mood and draw comparisons according to visuals. The more you record the more accurate your flows are. Get started today ! </p>
		    </div>

		    <div class="col-md-5">
	        <div class="img-responsive rounded featurette-image" style = "background-image: url('/img/monitoring.jpg');">
	        </div><!-- /img/monitoring.jpg -->
	      </div>
		  </div>
		  <hr>

		  <div class="row featurette">
		    <div class="col-md-7 mt-5">
					<h3 class="featurette-heading">Mental health. <span class="text-muted">Get Better.</span></h3>
					<p>Mental Health well-being is an important matter. We all experience difficult time and open up to someone should feel ashamed. Although  emotions might be a good way to start, we reckon seeking out for professional help if you feel you've been carrying an emotional burden for too long. Join the discussion can be game-changer !</p>
		    </div>
		    <div class="col-md-5">
	      	<div class="img-responsive rounded featurette-image" style = "background-image: url('/img/mentalhealth.jpg')"></div><!-- MoodTracker/public/img/mentalhealth.jpg -->
	      </div>
			</div>    
	    <hr id = "about-us">
	 	</div>
	</section>

	<section>
		<div class="jumbotron jumbotron-fluid jumbotron-activity mt-5">
		  <div class="container">
		    <h1 class="display-4 text-white">How was your week ?</h1>
		    <p class="lead text-white">Suscribe to our newsletter and get your weekly stats straight in your inbox.</p>
		    <p><a class="btn btn-warning mt-3 text-white px-4" href="#" role="button"> Suscribe</a></p>
		  </div>
		</div>
	</section>

	<section>
		<div class = "container team-section" >
		<!-- 	<div class = "ro" -->
			<div class = "row justify-content-center mb-4">
				<div class = "col-xs-10 col-xl-8 text-center">
					<h2 class = "text-secondary">Our team</h2>
					<br>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
				</div>
			</div>

		<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
		  <ol class="carousel-indicators">
		    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
		    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
		    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
		  </ol>
				  <div class="carousel-inner mt-5">
				    <div class="carousel-item active">
							<div class="row text-center">
			          <div class="col-lg-4 text-center">
			            <div class="rounded-circle mb-4 img-responsive img-team text-center" alt="Generic placeholder image" style = "background-image:url('/img/team1.jpg'); ;"></div> <!-- /img/team1.jpg -->
			            <h4>CEO</h4>
			            <p class = "text-justify">Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus</p>
			            <p><a class="btn btn-outline-info" href="#" role="button"> Contact »</a></p>
			          </div><!-- /.col-lg-4 -->
			          <div class="col-lg-4">
									<div class="rounded-circle mb-4 img-team  img-responsive" " alt="Generic placeholder image" style = "background-image:url('/img/team5.jpg');"></div> 
			            <h4>Chief Financial Officer</h4> 
			            <p class = "text-justify">Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus</p>
			              <p><a class="btn btn-outline-info" href="#" role="button"> Contact »</a></p>
			          </div><!-- /.col-lg-4 -->
			          <div class="col-lg-4">
			            <div class="rounded-circle mb-4 img-team img-responsive" " alt="Generic placeholder image" style = "background-image:url('/img/team6.jpg');"></div>
			            <h4>Chief Technical Officer</h4>
			            <p class = "text-justify">Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
			              <p><a class="btn btn-outline-info" href="#" role="button"> Contact »</a></p>
			          </div><!-- /.col-lg-4 -->
			        </div>
				    </div>
				    
				    <div class="carousel-item">
				      <div class="row">
			          <div class="col-lg-4">
			            <div class="rounded-circle mb-4 img-team img-responsive" " alt="Generic placeholder image " style = "background-image:url('/img/team3.jpg');"></div>
			            <h4>Chief Product Officer</h4>
			            <p class = "text-justify">Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus</p>
			             <p><a class="btn btn-outline-info" href="#" role="button"> Contact »</a></p>
			          </div><!-- /.col-lg-4 -->
			          <div class="col-lg-4">
			            <div class="rounded-circle mb-4 img-team img-responsive" " alt="Generic placeholder image" style = "background-image:url('/img/team4.jpg');"></div>
			            <h4>Chief Commercial Officer</h4>
			            <p class = "text-justify">Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Cras mattis consectetur purus sit amet fermentum. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh.</p>
			             <p><a class="btn btn-outline-info" href="#" role="button"> Contact »</a></p>
			          </div><!-- /.col-lg-4 -->
			          <div class="col-lg-4">
			              <div class="rounded-circle mb-4 img-team img-responsive" " alt="Generic placeholder image" style = "background-image:url('/img/team2.jpg');"></div>
			            <h4>Customer Success Manager</h4>
			            <p class = "text-justify">Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
			            <p><a class="btn btn-outline-info" href="#" role="button"> Contact »</a></p>
			          </div><!-- /.col-lg-4 -->
			        </div>
				    </div>
				    <div class="carousel-item">
				      <div class="row">
			          <div class="col-lg-4">
			              <div class="rounded-circle mb-4 img-team img-responsive text-align-center" " alt="Generic placeholder image" style = "background-image:url('/img/team7.jpg');"></div>
			            <h4>Chief Communication Officer</h4>
			            <p class = "text-justify">Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus</p>
			             <p><a class="btn btn-outline-info" href="#" role="button"> Contact »</a></p>
			          </div><!-- /.col-lg-4 -->
			          <div class="col-lg-4">
			              <div class="rounded-circle mb-4 img-team img-responsive" " alt="Generic placeholder image" style = "background-image:url('/img/team8.jpg');"></div>
			            <h4>Intern in Communication</h4>
			            <p class = "text-justify">Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus</p>
			             <p><a class="btn btn-outline-info" href="#" role="button"> Contact »</a></p>
			          </div><!-- /.col-lg-4 -->
			          <div class="col-lg-4">
			              <div class="rounded-circle mb-4 img-team img-responsive text-align-center" alt="Generic placeholder image" style = "background-image:url('/img/team9.jpg');"></div>
			            <h4>Developer Team</h4>
			            <p class = "text-justify">Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
			             <p><a class="btn btn-outline-info" href="#" role="button"> Contact »</a></p>
			          </div><!-- /.col-lg-4 -->
			        </div>
				    </div>
				  </div>
				  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev" >
				    <span class="carousel-control-prev-icon text-secondary" aria-hidden="true"></span>
				    <span class="sr-only" >Previous</span>
				  </a>
				  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
				    <span class="carousel-control-next-icon" aria-hidden="true"></span>
				    <span class="sr-only">Next</span>
				  </a>
				</div>	
			</div>
		</div>
	</section>

	 <hr class = "col-5 my-5"id = "contact">

	<section>
	 <div class = "container contact-section" >
		<div class="row featurette my-5">
	    <div class="col-md-6 col-lg-5 order-md-2 mt-lg-5 love-to".>
	      <h2 class="featurette-heading">Love to hear from you</h2>
	      <p class = "mt-2 text-md-left text-xs-center text-sm-center">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
	      </p>
	      <p class = "mt-2 text-md-left text-xs-center text-sm-center">
	      	<span>emotions</span><br>
					10 Witan Street<br>
					E26FG Bethnal Green, London<br>
					United Kingdom<br>
					+442041345678<br>
					hello@emotions.com<br>
				</p>
	    </div>
	 
	    
	    <div class="col-md-6 col-lg-5">
	      <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2482.335926153788!2d-0.05838858422935551!3d51.525398179638074!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48761cd078b4754d%3A0x5a7dddf88a0cda39!2sWitan%20St%2C%20London%2C%20Royaume-Uni!5e0!3m2!1sfr!2sfr!4v1570262629038!5m2!1sfr!2sfr" allowfullscreen=""></iframe>
	      </div><!-- /img/monitoring.jpg -->
	    </div>
	  </div>
	 </div>	
	<section>
</div>



<?php include('../app/views/include/footer.php'); ?>
<?php include('../app/views/include/layout_footer.php'); ?>





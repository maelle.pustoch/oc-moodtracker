<?php if(!isset($_SESSION['user_id'])) { ?>
<?php include('../app/views/include/layout_header.php'); ?>
<?php include('../app/views/include/menuTopBar.php'); ?>


<div class = "container-fluid p-0">

  <?php if(isset($_SESSION['useralreadyexists'])) {?>
    <div class = "d-row d-flex justify-content-end mt-5">
      <div class="col-lg-4 col-xl-4 col-md-5 col-sm-10 position-absolute alert alert-danger mr-2 mt-5" role="alert">
        
    <?php echo $_SESSION['useralreadyexists']; unset($_SESSION['useralreadyexists']); ?> 
      </div>
    </div>
<?php } ?>


  <?php if(isset($_SESSION['errorpasswordmatch'])) {?>
    <div class = "d-row d-flex justify-content-end mt-5">
      <div class="col-lg-4 col-xl-4 col-md-5 col-sm-10 position-absolute alert alert-danger mr-2 mt-5" role="alert">
       <?php echo $_SESSION['errorpasswordmatch']; unset($_SESSION['errorpasswordmatch']); ?>
      </div>
     </div>
  <?php } ?>



  <div class = "d-row d-flex">
    <div class = "col-md-4 col-lg-3 col-xl-3 p-0 m-0 img-bg-signup sign-up-section d-none d-sm-none d-md-block d-lg-block d-xl-block position-absolute"></div> 
  
  <div class = "col-md-8 col-lg-5 col-xl-5 text-center mx-auto mt-5">
    <h1 class = "mt-5 pt-5">Join us !</h1>
    <br>
    <h5 class = "font-weight-bold text-secondary"> Happiness is just a mile away, so start your journey from today! </h5> 
    <form action="/user/signup" method="POST" class = "mx-auto">
      <div class="row name-section mt-5 text-center justify-content-center">
        <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 col-xl-5 mb-2">
          <input type="text" class="form-control text-center bg-transparent" placeholder="First name" name ="firstname"required>
        </div>

        <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 col-xl-5 text-center mb-2">
          <input type="text" class="form-control text-center bg-transparent" placeholder="Last name" name ="lastname" required>
        </div>
      </div>
      <div class = "row mail-confirm text-center justify-content-center">
        <div class = "col-xs-10 col-sm-10 col-md-10 col-lg-10 col-xl-10 ">
          <input type="email" class="form-control mt-4 text-center bg-transparent" id="exampleFormControlInput1" placeholder="name@example.com" name="email"required>
          <input type="text" class="form-control mt-4 text-center bg-transparent" id="colFormLabel" placeholder="Username" name = "username" required>
          <input type="password" class="form-control mt-4 text-center bg-transparent" id="inputPassword3" placeholder="Password" name = "password" required>
          <input type="password" class="form-control mt-4 text-center bg-transparent" id="inputPassword3" placeholder="Confirm Password" name ="passwordconfirm" required>
          <a class="d-inline my-5 text-decoration-none link-back text-secondary border-0 mt-5 px-5" name = "back" href = "/"> Back </a>
          <button type = "submit" class="col-5 d-inline ml-2 btn btn-warning my-5 text-center px-4" name = "sign-up" >Sign up</button>                     
        </div>
      </div>         
    </form>
  </div>
</div>

</div>



<?php } else { header('Location: /user/account'); } ?>


<?php include('../app/views/include/layout_footer.php'); ?>


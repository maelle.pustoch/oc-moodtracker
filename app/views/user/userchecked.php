<?php
include('../app/views/include/layout_header.php');
include('../app/views/include/menuTopBar.php');

error_reporting(E_ALL);?>

<div class = "container-fluid">
	<div class = "row img-bg">
		<div class = "col text-center text-white mt-5">
			<h1 class = "mt-5"> Thanks for registering </h1>
			<h3> Start tracking today </h3>
			<a class="btn btn-outline-light px-5 mt-5" href = "/home/index"> Homepage </a>
		</div>
	</div>
</div>

<?php include('../app/views/include/layout_footer.php'); ?>

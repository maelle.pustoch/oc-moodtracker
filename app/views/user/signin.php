<?php if(!isset($_SESSION['user_id'])) { ?>
<?php include('../app/views/include/layout_header.php'); ?>
<?php include('../app/views/include/menuTopBar.php'); ?>
     

<?php if(isset($_SESSION['userexists'])) {?>
<div class = "error-user-exists col mt-5 d-flex justify-content-end ">
  <div class=" alert alert-danger mt-3" role="alert">
   <?php echo $_SESSION['userexists']; unset($_SESSION['userexists']); ?>
  </div>
</div> 
<?php }?>



<section> 
  <div class = "row" id = "signin-page">
    <div class = "col-xs-12 col-sm-12 col-md-3 col-lg-3 col-xl-3 p-0 img-bg-signin sign-in-section d-none d-sm-none d-md-block d-lg-block d-xl-block rounded position-absolute">
    </div>
    <div class = "col-xl-5 col-lg-5 col-md-8 col-xs-10 col-sm-9 my-5 justify-content-center text-center mx-auto">
      <h1 class = "text-secondary mt-5"> Welcome back </h1>
      <br>
      <p class = "w-75 mx-auto"> We are glad to see you back, track your mood and get the latest updates by logging in to your account ! </p> 
      <form action="/user/signin" method="POST" class = "mx-auto text-center justify-content-center">
        <input type="text" class= "col-8 username-signin text-center form-control bg-transparent border-bottom mt-5" placeholder="Username" aria-label="Username" aria-describedby="basic-addon1" id ="username" name="username" required>
        <input type="password" class= "col-8 password-signin text-center form-control border-bottom bg-transparent mt-4" placeholder="Password" aria-label="Passsword" aria-describedby="basic-addon1" id="password" name="password" required>
        <br>

        <a class="d-inline my-5 text-decoration-none link-back text-secondary border-0 mt-5 px-5 u-pointer" name = "back" href = "/"> Back </a>
        <button type="submit shadow-sm" class="btn btn-outline-secondary pl-4 pr-4 my-5 sign-in" name = "sign-in" >Sign in</button>
        <br>
      </form>
      <p class = "my-3"> You don't have an account ? <a href= "/user/usersignup">Sign up </a>
    </div>
  </div>
</section>



<?php } else { header('Location: /user/account'); } ?>
<?php include('../app/views/include/layout_footer.php'); ?>
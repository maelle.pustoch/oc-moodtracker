<?php if(isset($_SESSION['user_id'])) {  ?>
<?php include('../app/views/include/layout_header.php'); ?>
<?php include('../app/views/include/topnav.php'); ?>

<nav>
  <div class = "float-left col-1 d-none d-sm-none d-md-none d-lg-block">
  <?php include('../app/views/include/sidenav.php'); ?>
  </div>
</nav>

<header>
  <div class = "container-fluid mt-2 mb-5 dashboard" >
    <div class = "row">
      <div class = "col-12 my-2">
        <h3 class = "display-4 mt-3" style = "letter-spacing: 0.1em;">Dashboard</h3>  
        <hr>
      </div>
    </div>
  </div>
</header>

<div class = "container-fluid">
<section>
  <div class = "row justify-content-center my-4">  
    <div class = "hello-section text-white col-xs-10 col-sm-10 col-md-11 col-lg-11 col-xl-2 mb-5 rounded d-sm-block d-md-none d-lg-none d-xl-none mb-2" style = "background-image: url(/img/gradient1.jpg); background-size: cover; background-repeat: no-repeat; ">
      <h3 class = "display-4 my-3">Hello <?php echo ''.$_SESSION['username']. ' !' ?></h3>
      <hr> 
      <p>Mental health is an important matter ! Get your weekly stats and our feel good tips straight in your inbox from today !</p>
      
    </div>
  
    <div class="container-fluid ">
      <div class ="row d-flex justify-content-center">
        <div class ="col-xs-auto col-sm-5 col-md-5 col-lg-5 col-xl-5 ">
          <h3 class="text-lg-left text-xl-left text-md-center text-sm-center"  id = "construct-title">Coming soon</h3>
          <p class="p-0 text-lg-left text-xl-left text-md-center text-sm-center" id = "construct-p"> Ooops, it seems we didn't collect enough data from you yet ! This chart will be available soon...</p>
           <button type="button" class="btn btn-info mt-5" href = '/' id= "constructbtn">Home</button>
        </div>
        <div class ="col-xs-11 col-sm-5 col-md-5 col-lg-5 col-xl-5 ">
          <img src="/img/inconstruct.jpg" id = "inconstruct" class="img-fluid mt-5"  alt="in-construct-picture">
        </div>
      </div>
    </div>

    <div class = "chart-3 mt-2 rounded bg-white col-xs-11 col-sm-11 col-lg-11 col-xl-11 shadow-sm" id="chartline">
      <?php include('../app/views/user/chart-3.html'); ?>
    </div>
  </div>

</section>

<section>
<div class="container-fluid mt-5">
  <div class ="row d-flex justify-content-center mt-sm-2">
    <div class = "col-xs-auto col-sm-5 col-md-10 col-lg-5 col-xl-5 mt-5" id="chartLine2row">
      <h5 class= "graph-title2 graphtitle text-secondary text-center mb-3" id ="graph-title2">Monthly comparison</h5>
      <?php include('../app/views/user/chart-2.html'); ?> <!-- Polar -->
    </div>

    <div class = "col-xs-auto col-sm-5 col-md-10 col-lg-5 col-xl-5 mt-5" id="chartLine1">
      <h5 class= "mb-3 graphtitle graph-title text-center text-secondary" id ="graph-title1">Global comparison</h5>
      <?php include('../app/views/user/chart-1.html'); ?> <!-- Polar -->
    </div>

    <div class ="col-xs-auto col-sm-5 col-md-10 col-lg-5 col-xl-5 mt-5 align-middle">
      <h3 class="text-center" id = "construct-title2"> Coming soon</h3>
      <img src="/img/inconstruct2.jpg" id = "inconstruct2" class="img-fluid w-75 justify-content-center"  alt="in-construct-picture">
    </div>

    <div class ="col-xs-auto col-sm-5 col-md-10 col-lg-5 col-xl-5 mt-5">
      <h3 class="text-center" id = "construct-title1">Coming soon</h3>
      <img src="/img/inconstruct2.jpg" id = "inconstruct1" class="img-fluid w-75"  alt="in-construct-picture">
    </div>
    </div>
  </div>
</div>  
</section>
</div>


<?php } else { header('Location: /user/signin'); } ?>
<?php include('../app/views/include/layout_footer.php'); ?>

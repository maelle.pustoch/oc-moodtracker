
<nav class="navbar navbar-expand-lg fixed-top navbar-light " style = "background-color: rgba(255,255,255, 0.7)"; id = "navbar-front">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
 <!--  <a class="navbar-brand" href="#">Navbar</a> -->

  <div class="collapse navbar-collapse " id="navbarTogglerDemo03">
    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
      <li class="nav-item pr-4">
        <a class="nav-link text-upercase text-secondary" href="/" >Home</a>
      </li>
      <li class="nav-item pr-4">
        <a class="nav-link text-white text-secondary u-pointer" href="/user/useractivity" id ="menuactivity" >Activity</a>
      </li>
      <li class="nav-item pr-4">
        <a class="nav-link u-pointer " href="/user/useractivity#about-us" id = "menuaboutus">About Us</a>
      </li>
      <li class="nav-item pr-4">
        <a class="nav-link text-secondary u-pointer" href="/user/useractivity#contact" id = "menucontact" >Contact</a>
      </li>
      <li class="nav-item pr-4 d-sm-block d-md-block d-lg-none d-xl-none">
        <a class="nav-link text-info u-pointer" href = "/user/account" id = "menucontact" >Dashboard</a>
      </li>
      <li class="nav-item pr-4 d-sm-block d-md-block d-lg-none d-xl-none">
        <a class="nav-link text-info u-pointer" href = "/user/settings" id = "menucontact" >Settings</a>
      </li>
      <li class="nav-item pr-4 d-sm-block d-md-block d-lg-none d-xl-none">
        <a class="nav-link text-info u-pointer" href = "/user/logOut" id = "menucontact" >Log out</a>
      </li>
    </ul>

     <div class = "social text-decoration-none mr-5">
      <a class="text-info text-decoration-none fab fa-facebook-f pr-4 u-pointer" href = "http://www.facebook.com/maellelearnscode"></a>
      <a class="text-info text-decoration-none fab fa-twitter pr-4 u-pointer" href = "http://www.facebook.com/maellelearnscode/"></a>
      <a class="text-decoration-none text-info fab fa-instagram pr-4 u-pointer" href= "http://www.instagram.com/maellerussell" /></a>


    </div>

     
      <?php if(isset($_SESSION['user_id'])) {  ?>
      <a href = '/user/account' class="text-info text-decoration-none"><?php echo '<i class="fas fa-user mr-1 text-info"></i> ' .$_SESSION['username'].  '';?></a>


  
    </div>

     <?php } else { 
      echo ' <div class = "d-flex flex-xs-column justify-content-center"> <a  class = "btn btn-outline-secondary border-0 btn-signin  px-4" href = "/user/usersignin">Sign in </a> <a class = "btn btn-warning btn-signup font-weight-bold ml-2 px-4" href = "/user/usersignup">Sign up </a> </div>'; } ?>
    </form>
  </div>
</nav>


<!--   -->
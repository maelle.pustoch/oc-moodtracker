<?php include('../app/views/include/layout_header.php'); ?>


  <div class = "row sidenav" style = "margin-top: -5px">
    <div class = "col m-0 p-0" id ="sidebar" style = "background-color: #6b7b99; border:none">
      <div class="wrapper d-none d-lg-block">
        <nav id="sidebar border-0 m-0 p-0" class = "m-0">


          <ul class="list-unstyled components text-center text-white justify-content-center">
          <li >
            <a href="/user/account" class= "d-flex d-row text-white pt-2" >
              <div class = "icon col d-flex justify-content-center">
                <img class = "text-white" src="/img/dashbo.svg" alt="dash" style = "height:70px;">
<!--                 <p class = "pt-4">Dashboard</p -->
              </div>
            </a>
          </li>
          <li>
            <a href="/user/settings" class= "d-flex justify-content-start text-white pt-2">
              <div class = "icon col d-flex justify-content-center">
                <img class = "text-white" src="/img/settingsbo.svg" alt="settings" style = "height:50px; color: white;">
<!--                 <p class = "pt-2 pl-3">Settings</p> -->
              </div>
            </a>
          </li>
          </ul>
        </nav>
      </div>
    </div>
  </div>


<?php include('../app/views/include/layout_footer.php'); ?>

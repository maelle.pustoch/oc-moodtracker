

<!--Navbar -->
<nav class="mb-1 navbar navbar-expand-lg navbar-dark shadow-sm" style = "background-color: rgba(149,170,211);" id = "navbar-dashboard">
  <a class="navbar-brand" href="/" style = "font-family: 'DM Serif Display'; font-size:1.5em;" href="#">emotions</a> 
  
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-555"
    aria-controls="navbarSupportedContent-555" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  
  <div class="collapse navbar-collapse offset-1" id="navbarSupportedContent-555">
    <ul class="navbar-nav ml-auto nav-flex-icons">
      <li class="nav-item avatar">
        <a class="nav-link p-0" href="#">         
          <p class = "d-inline mr-3 text-white text-lowercase">
            <?php if(isset($_SESSION['userlog'])) { echo '<i class="far fa-smile"></i> Hello ' .$_SESSION['username']. '' ; } else { echo 'Log in to your account';}?>
          </p><br>  
      </li>
      <li class="nav-item avatar">
        <a class="d-inline mr-2 ml-4 text-decoration-none mr-3 d-sm-none d-md-none text-white" href='/'>Home</a>
        <a class = "d-inline d-lg-none d-xl-none text-decoration-none text-white mr-3" href = "/user/settings">Settings</a>
        <a class = "d-inline text-decoration-none text-white d-lg-none d-xl-none mr-3" href = "/user/account">Dashboard</a>
        <a class = "d-inline text-decoration-none text-warning" href = "/user/logOut">Log out</a>
        </span>        
      </li>
    </ul>
  </div>
</nav>



<!--   -->



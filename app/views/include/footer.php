

<!-- Footer -->
<footer class="page-footer font-small pt-4 text-white" style = "background: #366381; margin-top:20%" id = "footer">
  <div class="container-fluid text-center text-md-left">
    <div class="row">

      <!-- Grid column -->
      <div class="col-md-6 col-xl-6 col-lg-6 mx auto mt-md-0 mt-3">
        <h5 class="">emotions</h5>
        <p class = "w-75">Monitor your Mood and track your progresses. Check your latest progresses by <b>loggin in </b> or create an account and  <b>join us today</b></p><br>
        <button type="button" class="btn btn-light px-4 mb-5">Sign up</button>
      </div>
      <hr class="clearfix w-100 d-md-none pb-3">
      <div class="col-md-3 mb-md-0 mb-3 text-white">
        <h5 class="text-uppercase">Links</h5>
        <ul class="list-unstyled text-white">
          <li>
            <a class="text-white u-pointer" href="/home/index" >Home</a>
          </li>
          <li>
             <a class="text-white u-pointer" href="/user/useractivity" id ="menuactivity" >Activity</a>
          </li>
          <li>
            <a a class="text-white u-pointer" href="/user/useractivity#about-us">About us</a>
          </li>
          <li>
            <a a class="text-white u-pointer" href="/user/useractivity#contact">Contact</a>
          </li>
        </ul>
      </div>
      <!-- Grid column -->

      <!-- Grid column -->
            <div class="col-md-2 col-lg-2 text-center mx-auto my-4">

        <!-- Social buttons -->
        <h5 class="font-weight-bold text-uppercase mb-4">Follow Us</h5>

        <!-- Facebook -->
        
          <i class="fab fa-facebook-square"></i>
       
        <!-- Twitter -->
      
           <i class="fab fa-twitter pl-1" ></i>
      
        <!-- Google +-->
       
           <i class="fab fa-linkedin-in pl-1"></i>
        
        <!-- Dribbble -->
       
          <i class="fab fa-dribbble"></i>

          <i class="fab fa-behance"></i>
        </a>
      </div>
    </div>
  </div>
  <!-- Footer Links -->

  <!-- Copyright -->
  <div class="footer-copyright text-center py-3" style = "background: #30536A;">© 2019 Copyright: Maëlle Pustoc'h
   
  </div>
</footer>
<!-- Footer -->



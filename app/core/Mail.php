<?php

require('../vendor/PHPMailer/PHPMailer.php');
require('../vendor/PHPMailer/SMTP.php');
require('../config.php');




class Mail {

    private $phpMailer;

	function __construct() {

    global $usernameMail, $passwordMail;
    $this->phpMailer = new \PHPMailer\PHPMailer\PHPMailer();
		// $this->phpMailer->SMTPDebug = \PHPMailer\PHPMailer\SMTP::DEBUG_SERVER;                      // Enable verbose debug output
    $this->phpMailer->isSMTP();                                            // Send using SMTP
    $this->phpMailer->Host       = 'smtp.gmail.com';                    // Set the SMTP server to send through
    $this->phpMailer->SMTPAuth   = true;                                   // Enable SMTP authentication
    $this->phpMailer->Username   = $usernameMail;                     // SMTP username
    $this->phpMailer->Password   = $passwordMail;                               // SMTP password
    $this->phpMailer->SMTPSecure = \PHPMailer\PHPMailer\PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` also accepted
    $this->phpMailer->Port       = 587;                                // TCP port to connect to

    //Recipients
    $this->phpMailer->setFrom('maelle.pustoch@gmail.com', 'Objet');
    $this->phpMailer->isHTML(true); 
	 }


	 public function send($recipient, $subject, $message) {
	$this->phpMailer->addAddress($recipient);
	$this->phpMailer->Subject = $subject;
    $this->phpMailer->Body    = $message;
    $this->phpMailer->AltBody = 'Your client cannot read HTML messages';
    $this->phpMailer->send();

	 }
}


// $mail = new Mail();
// $mail->send('to', 'subject', 'message');